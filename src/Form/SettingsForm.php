<?php

namespace Drupal\login_message\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Configure the login message for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Allowed tags in the login message.
   */
  public const LOGIN_MESSAGE_ALLOWED_TAGS = [
    'a',
    'em',
    'strong',
    'ul',
    'ol',
    'li',
    'p',
  ];

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'login_message_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['login_message.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('login_message.settings');

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#description' => $this->t('The message to be displayed. Allowed HTML tags: @tags', ['@tags' => '<' . implode('>, <', static::LOGIN_MESSAGE_ALLOWED_TAGS) . '>']),
      '#maxlength' => 1024,
      '#default_value' => $settings->get('message'),
    ];

    $form['message_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Message type'),
      '#required' => TRUE,
      '#options' => [
        MessengerInterface::TYPE_STATUS => $this->t('Status message'),
        MessengerInterface::TYPE_WARNING => $this->t('Warning messsage'),
        MessengerInterface::TYPE_ERROR => $this->t('Error message'),
      ],
      '#default_value' => $settings->get('message_type'),
    ];

    $all_roles = $roles = array_map(['\Drupal\Component\Utility\Html', 'escape'], user_role_names(TRUE));
    $form['roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles'),
      '#description' => $this->t('Display the message only to users of the selected roles. Leave blank to display to any user.'),
      '#default_value' => $settings->get('roles') ?: [],
      '#options' => $all_roles,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('login_message.settings')
      ->set('message', $form_state->getValue('message'))
      ->set('message_type', $form_state->getValue('message_type'))
      ->set('roles', array_filter($form_state->getValue('roles') ?: []))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
